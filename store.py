from pymemcache.client import base
from pymemcache.exceptions import MemcacheError

CONFIG = {
    'SERVER': 'localhost',
    'PORT': 11211
}


class Store(object):
    def __init__(self, config):
        self.cache = base.Client((config['SERVER'],
                                 config['PORT']),
                                 connect_timeout=10,
                                 timeout=10)

    def cache_get(self, key):
        try:
            result = self.cache.get(key)
        except MemcacheError as err:
            print 'error while getting val from cache: %s' % err.message     
        return result
    
    def set_cache(self, key, value, expire):
        self.cache.set(key, value, expire)
