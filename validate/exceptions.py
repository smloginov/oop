class ValidationError(Exception):

    def __init__(self, message, code=None):
        super(ValidationError, self).__init__(message, code)

    def __repr__(self):
        return 'ValidationError(%s)' % self