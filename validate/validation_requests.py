from validate.validation_fields import *


class RequestMetaClass(type):
    def __new__(cls, clsname, bases, attrs):

        current_fields = []
        for key, value in list(attrs.items()):
            if isinstance(value, Field):
                current_fields.append((key, value))
                value.name = key
        attrs['declared_fields'] = dict(current_fields)

        new_cls = type.__new__(cls, clsname, bases, attrs)

        declared_fields = {}
        for base in reversed(new_cls.__mro__):
            if hasattr(base, 'declared_fields'):
                declared_fields.update(base.declared_fields)

        new_cls.fields = declared_fields

        return new_cls


class Request(object):
    __metaclass__ = RequestMetaClass

    def __init__(self):
        self._errors = {}

    @property
    def errors(self):
        return self._errors

    @property
    def is_valid(self):
        return not self._errors

    def validate(self, json_data):
        for name, field in self.fields.items():
            try:
                value = json_data.get(name, None)
                value = field.process_raw_data(value)
            except ValidationError as e:
                self._errors[name] = e
            else:
                setattr(self, name, value)
