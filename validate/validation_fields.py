import datetime
import json
from validate.exceptions import ValidationError

UNKNOWN = 0
MALE = 1
FEMALE = 2
GENDERS = {
    UNKNOWN: "unknown",
    MALE: "male",
    FEMALE: "female",
}


class Descriptor(object):

    def __get__(self, instance, cls):
        if instance is None:
            return self
        return instance.__dict__[self.name]

    def __set__(self, instance, value):
        instance.__dict__[self.name] = value


class Field(Descriptor):

    default_error_messages = {
        'nullable': 'field must not be null',
        'required': 'field required, but not in request'
    }

    empty_values = [{}, [], '']

    def __init__(self, required=True,
                 nullable=True,
                 error_messages=None):

        self.required = required
        self.nullable = nullable

        messages = {}
        for cl in reversed(self.__class__.__mro__):
            messages.update(getattr(cl, 'default_error_messages', {}))

        messages.update(error_messages or {})
        self.error_messages = messages

    def parse_to_value(self, value):
        if value is None and self.required:
            raise ValidationError(self.error_messages['required'],
                                  code='required')
        return value

    def validate(self, value):
        if value in self.empty_values and not self.nullable:
            raise ValidationError(self.error_messages['nullable'],
                                  code='nullable')

    def process_raw_data(self, value):
        value = self.parse_to_value(value)
        if value is not None:
            self.validate(value)
        return value


class CharField(Field):

    default_error_messages = {
        'type_error': 'must be string'
    }

    def validate(self, value):
        super(CharField, self).validate(value)
        if not isinstance(value, str):
            raise ValidationError(self.error_messages['type_error'],
                                  code='type_error')


class EmailField(CharField):

    default_error_messages = {
        'type_error': 'must be valid email'
    }

    def validate(self, value):
        super(EmailField, self).validate(value)
        if '@' not in value:
            raise ValidationError(self.error_messages['type_error'],
                                  code='type_error')


class PhoneField(Field):

    default_error_messages = {
        'type_error': 'must be valid phone'
    }

    def validate(self, value):
        super(PhoneField, self).validate(value)
        phone = str(value)
        if not isinstance(value, (int, str)) or not phone.isdigit() \
            or not len(phone) == 11 or not phone.startswith("7"):
            
            raise ValidationError(self.error_messages['type_error'],
                                  code='type_error')


class DateField(Field):

    default_error_messages = {
        'type_error': 'must be valid date'
    }

    def parse_to_value(self, value):
        value = super(DateField, self).parse_to_value(value)
        if value is None:
            return
        try:
            return datetime.datetime.strptime(value, '%d.%m.%Y')
        except (ValueError, TypeError):
            raise ValidationError(self.error_messages['type_error'],
                                  code='type_error')


class BirthDayField(DateField):

    default_error_messages = {
        'invalid': 'sorry for agiesm'
    }

    def calc_age(self, birthday):
        today = datetime.datetime.today()
        return (today - birthday).days / 365.25

    def validate(self, birthday):
        super(BirthDayField, self).validate(birthday)
        if self.calc_age(birthday) > 70:
            raise ValidationError(self.error_messages['invalid'],
                                  code='invalid')


class ArgumentsField(Field):

    default_error_messages = {
        'invalid': 'invalid arguments'
    }

    def validate(self, value):
        super(ArgumentsField, self).validate(value)
        try:
            json.dumps(value)
        except ValueError:
            raise ValidationError(self.error_messages['invalid'],
                                  code='invalid')


class GenderField(Field):

    default_error_messages = {
        'invalid': 'invalid gender'
    }

    def parse_to_value(self, value):
        value = super(GenderField, self).parse_to_value(value)
        if value is None:
            return
        if value not in (UNKNOWN, MALE, FEMALE):
            raise ValidationError(self.error_messages['invalid'],
                                  code='invalid')

        return GENDERS[value]


class ClientIDsField(Field):

    default_error_messages = {
        'invalid': 'invalid client ids'
    }

    def validate(self, value):
        super(ClientIDsField, self).validate(value)
        if not isinstance(value, list) or not all([isinstance(el, int) for el in value]):
            raise ValidationError(self.error_messages['invalid'],
                                  code='invalid')
