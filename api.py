#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import datetime
import logging
import hashlib
import uuid
from optparse import OptionParser
from BaseHTTPServer import HTTPServer, BaseHTTPRequestHandler
from scoring import get_interests, get_score
from store import Store

SALT = "Otus"
ADMIN_LOGIN = "admin"
ADMIN_SALT = "42"
OK = 200
BAD_REQUEST = 400
FORBIDDEN = 403
NOT_FOUND = 404
INVALID_REQUEST = 422
INTERNAL_ERROR = 500
ERRORS = {
    BAD_REQUEST: "Bad Request",
    FORBIDDEN: "Forbidden",
    NOT_FOUND: "Not Found",
    INVALID_REQUEST: "Invalid Request",
    INTERNAL_ERROR: "Internal Server Error",
}
UNKNOWN = 0
MALE = 1
FEMALE = 2
GENDERS = {
    UNKNOWN: "unknown",
    MALE: "male",
    FEMALE: "female",
}


class Descriptor(object):
    def __init__(self, required, nullable, name=None, **opts):
        self.name = name
        self.nullable = nullable
        self.required = required
        self.__dict__.update(opts)

    def __get__(self, instance, cls):
        if instance is None:
            return self
        else:
            if self.name in instance.__dict__:
                return instance.__dict__[self.name]
            else:
                return None

    def __set__(self, instance, value):
        if not self.nullable:
            if value is None or str(value) in ('', '{}', '[]'):
                raise ValueError('field %s must be initialize!' % self.name)
        instance.__dict__[self.name] = value


class Typed(Descriptor):
    expected_type = type(None)

    def __set__(self, instance, value):
        if not isinstance(value, self.expected_type):
            raise TypeError('expected %s in %s ' % (str(self.expected_type), type(instance).__name__))
        
        super(Typed, self).__set__(instance, value)


class Date(Descriptor):
    def __set__(self, instance, value):
        date = datetime.datetime.strptime(value, '%d.%m.%Y')
        
        super(Date, self).__set__(instance, date)


class Email(Descriptor):
    def __set__(self, instance, value):
        if '@' not in value:
            raise ValueError('validating error in mail field')
        
        super(Email, self).__set__(instance, value)


class Phone(Descriptor):
    def __set__(self, instance, value):
        value_str = str(value)
        if not value_str.startswith("7") or not len(value_str) == 11:
            raise ValueError('validating error in phone field')
        
        super(Phone, self).__set__(instance, value)


class ClientIds(Descriptor):
    def __set__(self, instance, value):
        for v in value:
            if not isinstance(v, int):
                raise ValueError('validating error in clientIds field')

        super(ClientIds, self).__set__(instance, value)


class BirthdayDate(Descriptor):
    def __set__(self, instance, value):
        birthday_date = datetime.datetime.strptime(value, '%d.%M.%Y')
        if datetime.date.today().year - birthday_date.year > 70:
            raise ValueError('wow, you are older than my grandma!')
        
        super(BirthdayDate, self).__set__(instance, birthday_date)


class Gender(Descriptor):
    def __set__(self, instance, value):
        if value not in (0, 1, 2):
            raise ValueError('validating error in gender field')
        
        super(Gender, self).__set__(instance, GENDERS[value])


class Integer(Typed):
    expected_type = int


class String(Typed):
    expected_type = str


class Dictionary(Typed):
    expected_type = dict


class List(Typed):
    expected_type = list


class CharField(String):
    pass


class PhoneField(Phone):
    pass


class EmailField(String, Email):
    pass


class ArgumentsField(Dictionary):
    pass


class BirthDayField(String, BirthdayDate):
    pass


class DateField(String, Date):
    pass


class ClientIDsField(List, ClientIds):
    pass


class GenderField(Integer, Gender):
    pass


class CheckedMeta(type):
    def __new__(cls, clsname, bases, methods):
        for key, value in methods.items():
            if isinstance(value, Descriptor):
                value.name = key
        return type.__new__(cls, clsname, bases, methods)


class Request(object):
    __metaclass__ = CheckedMeta

    def __init__(self, init_data):
        for field in init_data:
            setattr(self, field, init_data[field])

    @classmethod
    def read_json(cls, json_request):
        clsdict = dict(cls.__dict__)
        for _, v in clsdict.items():
            if isinstance(v, Descriptor):
                if v.required and v.name not in json_request:
                    raise KeyError("%s required, but missing in request" % v.name)

        return cls(json_request)


class ClientsInterestsRequest(Request):
    client_ids = ClientIDsField(required=True, nullable=False)
    date = DateField(required=False, nullable=True)


class OnlineScoreRequest(Request):
    first_name = CharField(required=False, nullable=True)
    last_name = CharField(required=False, nullable=True)
    email = EmailField(required=False, nullable=True)
    phone = PhoneField(required=False, nullable=True)
    birthday = BirthDayField(required=False, nullable=True)
    gender = GenderField(required=False, nullable=True)

    def __init__(self, init_data):

        super(OnlineScoreRequest, self).__init__(init_data)

        non_empty_fields = set()
        for field in init_data:
            if str(init_data[field]):
                non_empty_fields.add(field)

        if (set(['phone', 'email']).issubset(non_empty_fields)) or \
           (set(['first_name', 'last_name']).issubset(non_empty_fields)) or \
           (set(['gender', 'birthday']).issubset(non_empty_fields)):
            setattr(self, 'has', list(non_empty_fields))
        else:
            raise ValueError('validating error in online score request')


class MethodRequest(Request):
    account = CharField(required=False, nullable=True)
    login = CharField(required=True, nullable=True)
    token = CharField(required=True, nullable=True)
    arguments = ArgumentsField(required=True, nullable=True)
    method = CharField(required=True, nullable=False)

    @property
    def is_admin(self):
        return self.login == ADMIN_LOGIN


def check_auth(request):
    if request.is_admin:
        digest = hashlib.sha512(datetime.datetime.now().strftime(
            "%Y%m%d%H") + ADMIN_SALT).hexdigest()
    else:
        digest = hashlib.sha512(
            request.account + request.login + SALT).hexdigest()
    if digest == request.token:
        return True
    return False


def method_handler(request, ctx, store):

    try:
        method_request = MethodRequest.read_json(request["body"])
    except (KeyError, ValueError, TypeError) as err:
        response = err.message
        code = INVALID_REQUEST
    else:
        if check_auth(method_request):
            if method_request.method == "online_score":
                try:
                    online_score_req = OnlineScoreRequest.read_json(method_request.arguments)
                except (KeyError, ValueError, TypeError) as err:
                    response = err.message
                    code = INVALID_REQUEST
                else:
                    code = OK
                    if method_request.is_admin:
                        response = {"score": 42}
                    else:
                        calc_score = get_score(store,
                                               online_score_req.phone,
                                               online_score_req.email,
                                               online_score_req.birthday,
                                               online_score_req.gender,
                                               online_score_req.first_name,
                                               online_score_req.last_name)
                        response = {"score": calc_score}
                        ctx["has"] = online_score_req.has

            if method_request.method == "clients_interests":
                try:
                    interests_req = ClientsInterestsRequest.read_json(method_request.arguments)
                except (KeyError, ValueError, TypeError) as err:
                    response = err.message
                    code = INVALID_REQUEST
                else:
                    code = OK
                    response = {}
                    client_ids = interests_req.client_ids
                    for cid in client_ids:
                        code, interests = get_interests(store, cid)
                        if code == 200:
                            response[str(cid)] = interests
                    
                    ctx["nclients"] = len(client_ids)

        else:
            response, code = ERRORS[FORBIDDEN], FORBIDDEN 
    
    return response, code


class MainHTTPHandler(BaseHTTPRequestHandler):
    router = {
        "method": method_handler
    }
    store = Store()

    def get_request_id(self, headers):
        return headers.get('HTTP_X_REQUEST_ID', uuid.uuid4().hex)

    def do_POST(self):
        response, code = {}, OK
        context = {"request_id": self.get_request_id(self.headers)}
        request = None
        try:
            data_string = self.rfile.read(int(self.headers['Content-Length']))
            request = json.loads(data_string)
        except:
            code = BAD_REQUEST

        if request:
            path = self.path.strip("/")
            logging.info("%s: %s %s" %
                         (self.path, data_string, context["request_id"]))
            if path in self.router:
                try:
                    response, code = self.router[path](
                        {"body": request, "headers": self.headers}, context, self.store)
                except Exception, e:
                    logging.exception("Unexpected error: %s" % e)
                    code = INTERNAL_ERROR
            else:
                code = NOT_FOUND

        self.send_response(code)
        self.send_header("Content-Type", "application/json")
        self.end_headers()
        if code not in ERRORS:
            r = {"response": response, "code": code}
        else:
            r = {"error": response or ERRORS.get(
                code, "Unknown Error"), "code": code}
        context.update(r)
        logging.info(context)
        self.wfile.write(json.dumps(r))
        return


if __name__ == "__main__":
    op = OptionParser()
    op.add_option("-p", "--port", action="store", type=int, default=8080)
    op.add_option("-l", "--log", action="store", default=None)
    (opts, args) = op.parse_args()
    logging.basicConfig(filename=opts.log, level=logging.INFO,
                        format='[%(asctime)s] %(levelname).1s %(message)s', datefmt='%Y.%m.%d %H:%M:%S')
    server = HTTPServer(("localhost", opts.port), MainHTTPHandler)
    logging.info("Starting server at %s" % opts.port)
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass
    server.server_close()
