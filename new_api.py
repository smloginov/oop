from validate.validation_fields import *

import json
import datetime
import logging
import hashlib
import uuid
from optparse import OptionParser
from BaseHTTPServer import HTTPServer, BaseHTTPRequestHandler
from scoring import get_interests, get_score

from validate.validation_requests import Request


SALT = "Otus"
ADMIN_LOGIN = "admin"
ADMIN_SALT = "42"
OK = 200
BAD_REQUEST = 400
FORBIDDEN = 403
NOT_FOUND = 404
INVALID_REQUEST = 422
INTERNAL_ERROR = 500
ERRORS = {
    BAD_REQUEST: "Bad Request",
    FORBIDDEN: "Forbidden",
    NOT_FOUND: "Not Found",
    INVALID_REQUEST: "Invalid Request",
    INTERNAL_ERROR: "Internal Server Error",
}
UNKNOWN = 0
MALE = 1
FEMALE = 2
GENDERS = {
    UNKNOWN: "unknown",
    MALE: "male",
    FEMALE: "female",
}


class ClientsInterestsRequest(Request):
    client_ids = ClientIDsField(required=True, nullable=False)
    date = DateField(required=False, nullable=True)


class OnlineScoreRequest(Request):
    first_name = CharField(required=False, nullable=True)
    last_name = CharField(required=False, nullable=True)
    email = EmailField(required=False, nullable=True)
    phone = PhoneField(required=False, nullable=True)
    birthday = BirthDayField(required=False, nullable=True)
    gender = GenderField(required=False, nullable=True)

    @property
    def nonempty_fields(self):
        return [name for name, _ in self.fields.items() if getattr(self, name, None)]

    def validate(self, json_data):
        super(OnlineScoreRequest, self).validate(json_data)
        if self.is_valid:
            if not ((self.phone and self.email)
                    or
                    (self.first_name and self.last_name)
                    or
                    (self.gender and self.birthday)):

                self._errors['common_errors'] = ValidationError('error validation in request', 'invalid')


class MethodRequest(Request):
    account = CharField(required=False, nullable=True)
    login = CharField(required=True, nullable=True)
    token = CharField(required=True, nullable=True)
    arguments = ArgumentsField(required=True, nullable=True)
    method = CharField(required=True, nullable=False)

    @property
    def is_admin(self):
        return self.login == ADMIN_LOGIN


def check_auth(request):
    if request.is_admin:
        digest = hashlib.sha512(datetime.datetime.now().strftime(
            "%Y%m%d%H") + ADMIN_SALT).hexdigest()
    else:
        digest = hashlib.sha512(
            request.account + request.login + SALT).hexdigest()
    if digest == request.token:
        return True
    return False


def online_score_handler(request, ctx, store, is_admin):
    online_score_request = OnlineScoreRequest()
    online_score_request.validate(request)
    if online_score_request.is_valid:
        phone = online_score_request.phone
        email = online_score_request.email
        first_name = online_score_request.first_name
        last_name = online_score_request.last_name
        gender = online_score_request.gender
        birthday = online_score_request.birthday

        score = 42 if is_admin else get_score(store, phone, email,
                                                birthday, gender,
                                                first_name, last_name)

        ctx["has"] = online_score_request.nonempty_fields
        return {"score": score}, OK

    return {"error": online_score_request.errors}, INVALID_REQUEST


def clients_interests_handler(request, ctx, store, is_admin):
    clients_interests_request = ClientsInterestsRequest()
    clients_interests_request.validate(request)
    if clients_interests_request.is_valid:
        clients_ids = clients_interests_request.client_ids
        response = {}
        for cid in clients_ids:
            interests = get_interests(store, cid)
            response[str(cid)] = interests
        ctx["nclients"] = len(clients_ids)
        return response, OK

    return {"error": clients_interests_request.errors}, INVALID_REQUEST


def method_handler(request, ctx, store):
    method_request = MethodRequest()
    method_request.validate(request["body"])
    if method_request.is_valid:
        if check_auth(method_request):
            try:
                response, code = router[method_request.method](method_request.arguments, ctx, store, method_request.is_admin)
            except KeyError:
                return ERRORS[INTERNAL_ERROR], INTERNAL_ERROR
        else:
            return ERRORS[FORBIDDEN], FORBIDDEN

    else:
        response = {"error": method_request.errors}
        code = INVALID_REQUEST

    return response, code


router = {
    "method": method_handler,
    "online_score": online_score_handler,
    "clients_interests": clients_interests_handler
}


class MainHTTPHandler(BaseHTTPRequestHandler):
    store = None

    def get_request_id(self, headers):
        return headers.get('HTTP_X_REQUEST_ID', uuid.uuid4().hex)

    def do_POST(self):
        response, code = {}, OK
        context = {"request_id": self.get_request_id(self.headers)}
        request = None
        try:
            data_string = self.rfile.read(int(self.headers['Content-Length']))
            request = json.loads(data_string)
        except:
            code = BAD_REQUEST

        if request:
            path = self.path.strip("/")
            logging.info("%s: %s %s" %
                         (self.path, data_string, context["request_id"]))
            if path in router:
                try:
                    response, code = router[path](
                        {"body": request, "headers": self.headers}, context, self.store)
                except Exception, e:
                    logging.exception("Unexpected error: %s" % e)
                    code = INTERNAL_ERROR
            else:
                code = NOT_FOUND

        self.send_response(code)
        self.send_header("Content-Type", "application/json")
        self.end_headers()
        if code not in ERRORS:
            r = {"response": response, "code": code}
        else:
            r = {"error": response or ERRORS.get(
                code, "Unknown Error"), "code": code}
        context.update(r)
        logging.info(context)
        self.wfile.write(json.dumps(r))
        return


if __name__ == "__main__":
    op = OptionParser()
    op.add_option("-p", "--port", action="store", type=int, default=8080)
    op.add_option("-l", "--log", action="store", default=None)
    (opts, args) = op.parse_args()
    logging.basicConfig(filename=opts.log, level=logging.INFO,
                        format='[%(asctime)s] %(levelname).1s %(message)s', datefmt='%Y.%m.%d %H:%M:%S')
    server = HTTPServer(("localhost", opts.port), MainHTTPHandler)
    logging.info("Starting server at %s" % opts.port)
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass
    server.server_close()
